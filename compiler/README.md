# C compiler with antlr4/c++

## Instructions

This minimal example shows how to build a basic C compiler implemented with the tools Antlr4 
and a C++ target. The only file the compiler can deal with is:

```
int main() {
   return n;
}
```
where `n` is a positive integer. 

## Files

- `ifcc.g4` contains the grammar in antlr4 format
- `main.cpp` contains the C++ code to call antlr4 chain with file name provided in the command 
   line and perform the visit of the parser tree
- `visitor.h` is the parse tree visitor that produces the ASM output   
- `Makefile` can be called to compile the parser (libraries and include directories default to
   the values used by the docker image)
- `compile.sh` is a script that compiles the parser in a docker environment
- `compile_if.sh` is a script that compiles the parser adapted to the IF department linux machines


