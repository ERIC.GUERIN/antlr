docker run --rm -v $(pwd):/work eguerin/antlr4cpp bash -c "cd /work; export INCDIR=/usr/include/antlr4-runtime; export ANTLR4=antlr4; export LIBDIR=/lib/x86_64-linux-gnu; make"
