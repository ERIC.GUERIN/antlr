#include <stdio.h>

void function(char a) {
   putchar(a);
}

int main() {
   function('a');
   return 0;
}
