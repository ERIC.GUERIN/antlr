Comptage des instructions
=========================

demo: taper `make` puis lire le makefile

# Intro

Objectif: valider le volet "optimisation" du PLD comp

Approche: compiler un même programme SANS puis AVEC optimisations, et comparer les "performances" à l'exécution

Implémentation: exécutions pas-à-pas dans GDB, et comptage des instructions

Note: Je sais bien que cette méthode est simpliste et ne reflète pas les "vraies" performances du programme, car elle ignore le pipeline, l'exécution dans le désordre, les latences mémoire, etc. Mais pour le PLD comp, ce n'est probablement pas grave.

# Remarques en vrac

Le makefile invoque gdb avec `-batch-silent`, mais on peut aussi préférer `-batch` qui est plus bavard (affiche le détail des instructions comptabilisées). 
Voire, on peut préférer interagir en direct avec GDB, c'est à dire taper manuellement les commandes `stepi` et `finish` pour suivre l'exécution au fur et à mesure.

Par souci de facilité d'interprétation des résultats, je ne regarde que les instructions "du programme" (i.e. celui qu'on vient de compiler), et j'ignore tout le reste: les fonctions de bibliothèque, les aspects "éditions de liens dynamique", le runtime C, etc.

Attention, ici je ne vérifie *pas* la correctitude des résulats. Autrement dit, on suppose que le programme optimisé a effectivement le bon comportement, et on vérifie seulement qu'il s'exécute plus vite.
