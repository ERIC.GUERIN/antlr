import sys
import gdb

# kludge to suppress the "Missing separate debuginfos" complaints
gdb.execute("set build-id-verbose 0")

gdb.execute("start") # i.e. run until the beginning of the `main()` function

instructions=0

while True:
    gdb.execute("x/i $pc")

    backtrace=[]
    frame = gdb.newest_frame()
    while frame:
        backtrace.append(str(frame.name())) # str() avoids NoneType cropping up in our list
        frame=frame.older()
    # print(backtrace)
    
    if 'main' not in backtrace:
        break
    elif any( (name[-4:] == '@plt') or (name[:4] == '_dl_') or (name == 'None') or (name[:2] == '__')
            for name in backtrace):
        print("PLD COMP: skipping library function call")
        gdb.execute("finish")
    else:
        gdb.execute("stepi")
        instructions+=1

print("instructions executed:",instructions,file=sys.stderr)
